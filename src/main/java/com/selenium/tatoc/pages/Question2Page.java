package com.selenium.tatoc.pages;

import java.util.ArrayList;


import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class Question2Page {
	WebDriver driver;
	
	
	public Question2Page(WebDriver driver) {
		this.driver=driver;
	}

	public void launchApplication() {
		System.setProperty("webdriver.chrome.driver","\\C:\\Users\\Rishabh\\Downloads\\webdriver\\chromedriver_win32\\chromedriver.exe\\");
	    driver = new ChromeDriver();
	  //  JavascriptExecutor js = (JavascriptExecutor)driver;
	    driver.manage().window().maximize();
	    driver.get("http://10.0.1.86/tatoc");
	    WebElement basicCourse=driver.findElement(By.xpath("//a[@href='/tatoc/basic']"));
	    JavascriptExecutor js = (JavascriptExecutor)driver;
	    js.executeScript("arguments[0].click();",basicCourse);
        String actual= ((JavascriptExecutor)driver).executeScript("return document.title;").toString();
        Assert.assertEquals(actual,"Grid Gate - Basic Course - T.A.T.O.C","title is not matched");
        }
	
	public void greenBox() {
		WebElement green = driver.findElement(By.className("greenbox"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",green);
		String actual=((JavascriptExecutor)driver).executeScript("return document.title;").toString();
		Assert.assertEquals(actual,"Frame Dungeon - Basic Course - T.A.T.O.C","title is not matched");
	}
	
	public void frameDungeon() {
		WebElement iframe1=driver.findElement(By.cssSelector("#main"));
			driver.switchTo().frame(iframe1);
		String box1 = driver.findElement(By.xpath("//div[@id='answer']")).getAttribute("class");	
		WebElement iframe2 = driver.findElement(By.cssSelector("#child"));
	        driver.switchTo().frame(iframe2);
		String  box2 =driver.findElement(By.cssSelector("#answer")).getAttribute("class");
		while(!box1.equals(box2)){
			driver.switchTo().parentFrame();
			WebElement repaintbtn=driver.findElement(By.xpath("//a[@onclick='reloadChildFrame();']"));
			((JavascriptExecutor)driver).executeScript("arguments[0].click();",repaintbtn);
			driver.switchTo().frame(iframe2);
			box2 = driver.findElement(By.cssSelector("#answer")).getAttribute("class");}
		driver.switchTo().parentFrame();
		WebElement proceed = driver.findElement(By.xpath("//a[@onclick='gonext();']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",proceed);
		String actual = ((JavascriptExecutor)driver).executeScript("return document.title;").toString();
		Assert.assertEquals(actual,"Drag - Basic Course - T.A.T.O.C");
		}
	
	public void dragAndDrop() {
		Actions action = new Actions(driver);
		WebElement sourceElement = driver.findElement(By.xpath("//div[@id='dragbox']"));
		WebElement targetElement = driver.findElement(By.xpath("//div[@id='dropbox']"));
		action.clickAndHold(sourceElement).moveToElement(targetElement).release().build().perform();
		WebElement proceed=driver.findElement(By.xpath("//a[@onclick='gonext();']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",proceed);
		String actual = ((JavascriptExecutor)driver).executeScript("return document.title;").toString();
		Assert.assertEquals(actual,"Windows - Basic Course - T.A.T.O.C","title is not matched");
	}
	public void popWindow() {
		WebElement launchPopWindow = driver.findElement(By.xpath("//a[@onclick='launchwindow();']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",launchPopWindow);
		switchWindow(1);
		 driver.findElement(By.cssSelector("#name")).sendKeys("rishabh");
		WebElement submit = driver.findElement(By.cssSelector("#submit"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",submit);
		switchWindow(0);
		WebElement proceed = driver.findElement(By.xpath("//a[@onclick='gonext();']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",proceed);
		String actual = ((JavascriptExecutor)driver).executeScript("return document.title;").toString();
		Assert.assertEquals(actual,"Cookie Handling - Basic Course - T.A.T.O.C");
	}
	public void switchWindow(int i) {
		 ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		 driver.switchTo().window(tabs.get(i));}
	
	
	public void cookieHandling() {
		WebElement generateToken = driver.findElement(By.xpath("//a[@onclick='generateToken();']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",generateToken);
		String value= driver.findElement(By.xpath("//span[@id=\"token\"]")).getText().substring(7);
		System.out.println(value);
		 driver.manage().addCookie(new Cookie("Token", value));
		 WebElement proceed = driver.findElement(By.cssSelector("body > div > div.page > a:nth-child(10)"));
		 ((JavascriptExecutor)driver).executeScript("arguments[0].click();",proceed);
	     driver.close();
		}

}
