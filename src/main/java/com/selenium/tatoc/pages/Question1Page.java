package com.selenium.tatoc.pages;

import java.util.ArrayList;

import org.openqa.selenium.By;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Question1Page {
	WebDriver  driver;
	
	public Question1Page(WebDriver driver) {
		this.driver=driver;
//		System.setProperty("webdriver.chrome.driver","\\C:\\Users\\Rishabh\\Downloads\\webdriver\\chromedriver_win32\\chromedriver.exe\\");
//	    driver = new ChromeDriver();
	}
	public void launchApplication() {
		System.setProperty("webdriver.chrome.driver","\\C:\\Users\\Rishabh\\Downloads\\webdriver\\chromedriver_win32\\chromedriver.exe\\");
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("http://10.0.1.86/tatoc");
	    driver.manage().window().maximize();
	    driver.findElement(By.xpath("//a[@href='/tatoc/basic']")).click();
        String actual=driver.getTitle();
        String expected = "Grid Gate - Basic Course - T.A.T.O.C";
        Assert.assertEquals(actual, expected);
        }

	public void greenBox() {
		driver.findElement(By.xpath("//div[@class='greenbox']")).click();
		String actual=driver.getTitle();
		String expected = "Frame Dungeon - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected);
	}
	public void frameDungeon() {
		WebElement iframe1=driver.findElement(By.cssSelector("#main"));
			driver.switchTo().frame(iframe1);
		String box1 = driver.findElement(By.xpath("//div[@id='answer']")).getAttribute("class");	
		WebElement iframe2 = driver.findElement(By.cssSelector("#child"));
	        driver.switchTo().frame(iframe2);
		String  box2 =driver.findElement(By.cssSelector("#answer")).getAttribute("class");
		//WebDriverWait wait=new WebDriverWait(driver,5);
		while(!box1.equals(box2)){
			driver.switchTo().parentFrame();
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > center > a:nth-child(7)"))).click();
			driver.findElement(By.xpath("//a[@onclick='reloadChildFrame();']")).click();
			driver.switchTo().frame(iframe2);
			box2 = driver.findElement(By.cssSelector("#answer")).getAttribute("class");}
		driver.switchTo().parentFrame();
		driver.findElement(By.xpath("//a[@onclick='gonext();']")).click();
		String actual = driver.getTitle();
		String expected = "Drag - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected);
		}
	
	public void dragAndDrop() {
		Actions action = new Actions(driver);
		WebElement sourceElement = driver.findElement(By.xpath("//div[@id='dragbox']"));
		WebElement targetElement = driver.findElement(By.xpath("//div[@id='dropbox']"));
		action.clickAndHold(sourceElement).moveToElement(targetElement).release().build().perform();
		driver.findElement(By.xpath("//a[@onclick='gonext();']")).click();
		String actual = driver.getTitle();
		String expected ="Windows - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected);
	}
	public void popWindow() {
		driver.findElement(By.xpath("//a[@onclick='launchwindow();']")).click();
		switchWindow(1);
		 driver.findElement(By.cssSelector("#name")).sendKeys("rishabh");
		driver.findElement(By.cssSelector("#submit")).click();
		switchWindow(0);
		driver.findElement(By.xpath("//a[@onclick='gonext();']")).click();
		
		String actual = driver.getTitle();
		String expected = "Cookie Handling - Basic Course - T.A.T.O.C";
		Assert.assertEquals(actual, expected);
	}
	private void switchWindow(int i) {
		 ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		 driver.switchTo().window(tabs.get(i));}
	
	
	public void cookieHandling() {
		driver.findElement(By.xpath("//a[@onclick='generateToken();']")).click();
		String value= driver.findElement(By.xpath("//span[@id=\"token\"]")).getText().substring(7);
		System.out.println(value);
		 driver.manage().addCookie(new Cookie("Token", value));
		 driver.findElement(By.cssSelector("body > div > div.page > a:nth-child(10)")).click();
	     driver.close();
		}

}
