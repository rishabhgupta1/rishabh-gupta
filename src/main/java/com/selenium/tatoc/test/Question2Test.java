package com.selenium.tatoc.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.selenium.tatoc.pages.Question2Page;

public class Question2Test {
	
	WebDriver driver ;
	Question2Page ques=new Question2Page(driver);
	
//	@BeforeTest
//	public void setUp(){
//		System.setProperty("webdriver.chrome.driver","\\C:\\Users\\Rishabh\\Downloads\\webdriver\\chromedriver_win32\\chromedriver.exe\\");
//	     driver = new ChromeDriver();
//	}
	@Test(priority=1)
	public void launchApplication() {
		ques.launchApplication();
 }
	@Test(priority=2)
	public void greenBox() {
		ques.greenBox();
	}
	@Test(priority=3)
	public void frameDungeon() {
		ques.frameDungeon();
		}
	@Test(priority=4)
	public void dragAndDrop() {
		ques.dragAndDrop();
	}
	@Test(priority=5)
	public void popWindow() {
		ques.popWindow();
	}
	@Test(priority=6)
	public void cookieHandling() {
		ques.cookieHandling();
		
	}
	@AfterTest
	public void termDown() {
		driver.quit();
	}
}

