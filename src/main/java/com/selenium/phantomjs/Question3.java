package com.selenium.phantomjs;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;


public class Question3 {
	 WebDriver driver = new PhantomJSDriver();
	@Test	
	 public void launchApplication() {
//		DesiredCapabilities caps = new DesiredCapabilities();
//        caps.setJavascriptEnabled(true); 
		 System.setProperty("phantomjs.binary.path","C:\\Users\\Rishabh\\Downloads\\webdriver\\phantomjs-2.1.1-windows\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		 WebDriver driver = new PhantomJSDriver();
		 driver.get("http://google.com");
		 Assert.assertEquals(driver.getTitle(),"Google","titile is not matched");}
	
	@Test
	public void searchFunctionality() {
		 WebElement element=driver.findElement(By.xpath("//input[@title='Search']"));
		 element.sendKeys("youtube");
		 String actual=driver.getTitle();
		 String expected = "(2719) YouTube";
		 Assert.assertEquals(actual, expected, "title is not matched");
	}

}
